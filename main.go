package main

import (
	"bytes"
	"compress/zlib"
	"encoding/binary"
	"errors"
	"flag"
	"fmt"
	"golang.org/x/text/encoding/charmap"
	"io"
	"os"
	"path/filepath"
	"strings"
)

type DCP struct {
	file    *os.File
	DirName string

	Files  []*DCPFile
	CurPos int
}

func (d *DCP) FileCount() int {
	return len(d.Files)
}

func (d *DCP) Unpack() ([]byte, error) {
	file := d.Files[d.CurPos]
	_, err := d.file.Seek(int64(file.Offset), io.SeekStart)
	if err != nil {
		return nil, err
	}
	defer func() { d.CurPos++ }()

	if !file.Compressed {
		uncomp := make([]byte, file.UncompressedSize)
		_, err := d.file.Read(uncomp)
		if err != nil {
			return nil, err
		}
		return uncomp, nil
	} else {
		uncomp := make([]byte, file.UncompressedSize)
		compdata := make([]byte, file.CompressedSize)
		_, err := d.file.Read(compdata)
		if err != nil {
			return nil, err
		}

		zreader, err := zlib.NewReader(bytes.NewReader(compdata))
		defer zreader.Close()
		if err != nil {
			return nil, err
		}

		zreader.Read(uncomp)
		return uncomp, nil
	}
}

type DCPFile struct {
	Path       string
	Compressed bool
	Offset     uint32

	UncompressedSize uint32
	CompressedSize   uint32
}

type Unpacker struct {
	file        *os.File
	tableOffset uint32
	Name        string
	fileCount   uint32
}

/*func (u *Unpacker) readString() (string, error) {
	strLen := make([]byte, 1)
	_, err := u.file.Read(strLen)
	if err != nil {
		return "", err
	}

	str := make([]byte, strLen[0])
	_, err = u.file.Read(str)
	if err != nil {
		return "", err
	}

	return string(str[:len(str)-1]), nil // Странный хак, обрезает у строки нуль терминатор
}*/

func (u *Unpacker) readString() ([]byte, error) {
	strLen := make([]byte, 1)
	_, err := u.file.Read(strLen)
	if err != nil {
		return nil, err
	}

	str := make([]byte, strLen[0])
	_, err = u.file.Read(str)
	if err != nil {
		return nil, err
	}

	return str[:len(str)-1], nil // Странный хак, обрезает у строки нуль терминатор
}

func (u *Unpacker) readAndDecodeString() ([]byte, error) {
	str, err := u.readString()
	if err != nil {
		return nil, err
	}
	decstr := u.decrypt(str)

	encoder := charmap.Windows1251.NewDecoder()
	enStr, err := encoder.Bytes(decstr)
	if err != nil {
		return nil, err
	}

	return enStr, nil
}

func (u *Unpacker) readNumber() (uint32, error) {
	num := make([]byte, 4)
	_, err := u.file.Read(num)
	if err != nil {
		return 0, err
	}

	return binary.LittleEndian.Uint32(num), nil
}

func (u *Unpacker) checkMagic() bool {
	u.file.Seek(0x00, io.SeekStart)

	magic := make([]byte, 4)
	u.file.Read(magic)

	return bytes.Equal(magic, []byte{0xDE, 0xAD, 0xC0, 0xDE})
}

func (u *Unpacker) readTable() error {
	_, err := u.file.Seek(0x80, io.SeekStart)
	if err != nil {
		return err
	}

	u.tableOffset, err = u.readNumber()
	if err != nil {
		return err
	}

	return nil
}

func (u *Unpacker) readName() error {
	_, err := u.file.Seek(int64(u.tableOffset), io.SeekStart)
	if err != nil {
		return err
	}

	name, err := u.readString()
	if err != nil {
		return err
	}
	u.Name = string(name)

	return nil
}

func (u *Unpacker) readFileCount() error {
	_, err := u.file.Seek(0x01, io.SeekCurrent)
	if err != nil {
		return err
	}

	u.fileCount, err = u.readNumber()
	if err != nil {
		return err
	}

	return nil
}

func (u *Unpacker) readFiles() ([]*DCPFile, error) {
	var files []*DCPFile
	for i := 0; i < int(u.fileCount); i++ {
		file := DCPFile{}

		/*		if offset, _ := u.file.Seek(0x00, io.SeekCurrent); offset > 0 {
				fmt.Printf("%X\n", offset)
		}*/

		str, err := u.readAndDecodeString()
		if err != nil {
			return nil, err
		}

		file.Path = strings.ReplaceAll(string(str), "\\", "/")

		offset, err := u.readNumber()
		if err != nil {
			return nil, err
		}
		file.Offset = offset

		uncsize, err := u.readNumber()
		if err != nil {
			return nil, err
		}
		file.UncompressedSize = uncsize

		compsize, err := u.readNumber()
		if err != nil {
			return nil, err
		}
		file.CompressedSize = compsize

		if compsize != 0x00 {
			file.Compressed = true
		}

		_, err = u.file.Seek(0x0C, io.SeekCurrent)
		if err != nil {
			return nil, err
		}

		files = append(files, &file)
	}
	return files, nil
}

func (Unpacker) decrypt(str []byte) []byte {
	var result []byte
	for _, v := range str {
		result = append(result, v^0x44)
	}

	return result
}

func (u *Unpacker) Unpack() (*DCP, error) {
	if !u.checkMagic() {
		return nil, errors.New("wrong magic")
	}
	if err := u.readTable(); err != nil {
		return nil, err
	}

	if err := u.readName(); err != nil {
		return nil, err
	}

	if err := u.readFileCount(); err != nil {
		return nil, err
	}

	files, err := u.readFiles()
	if err != nil {
		return nil, err
	}
	d := DCP{
		file:    u.file,
		Files:   files,
		DirName: u.Name,
	}

	return &d, nil
}

func NewUnpacker(file *os.File) *Unpacker {
	return &Unpacker{file: file}
}

const (
	fileArgUsage = "Specifies file name to unpack"
	//outArgUsage = "Указывает директорию для распаковки, по-умолчанию используется директория по имени файла"
)

func getArgs() (fileName string, outputDir string) {
	flag.StringVar(&fileName, "file", "", fileArgUsage)
	//flag.StringVar(&outputDir,"out", strings.TrimSuffix(fileName, ".dcp"), outArgUsage)
	flag.StringVar(&fileName, "f", "", fileArgUsage+" (shorthand)")
	//flag.StringVar(&outputDir,"o", strings.TrimSuffix(fileName, ".dcp"),
	//	outArgUsage + " (сокращение")

	flag.Parse()

	return fileName, outputDir
}

func main() {
	fmt.Print("undcp 1.0 by ъъъ (2021)\n\n")
	fileName, _ := getArgs()
	if fileName == "" {
		panic("file name not specified")
	}

	file, err := os.Open(fileName)
	defer file.Close()
	if err != nil {
		panic(err)
	}

	u := NewUnpacker(file)
	d, err := u.Unpack()
	if err != nil {
		panic(err)
	}

	err = os.Mkdir(d.DirName, os.ModePerm)
	if err != nil {
		panic(err)
	}
	err = os.Chdir(d.DirName)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Extracting %d Files", d.FileCount())
	for _, f := range d.Files {
		fmt.Printf("Extracing %s [%d on %d]\n", f.Path, d.CurPos+1, d.FileCount())

		dir, fname := filepath.Split(f.Path)
		var file *os.File
		if dir == "" {
			file, _ = os.Create(fname)
		} else {
			os.MkdirAll(dir, os.ModePerm)
			file, _ = os.Create(dir + fname)
		}

		data, _ := d.Unpack()
		file.Write(data)

		file.Close()

	}
	fmt.Printf("\nExtraction completed: extracted %d of %d Files\n", d.CurPos, d.FileCount())
}
